import math
from suffix_array import Suffix_Array
from viajante import Aligner
import sys
import os

#Se eliminan los archivos de salida si ya existen

if os.path.exists("alineamiento.fa"):
  os.remove("alineamiento.fa")

if os.path.exists("phix_salida.fa"):
  os.remove("phix_salida.fa")


#Se lee el genoma de referencia

fa = open("phix.fa", "r")
reader = fa.readlines()
g_ref = ""
for k,x in enumerate(reader):
    if k > 0 :
        g_ref += x[:-1]


#Se leen los reads

fa = open("reads_phix_1.fastq", "r")
reader = fa.readlines()
reads = []
for k,x in enumerate(reader):
    if k % 4 == 1 :
        reads.append(x[:-1])


#Se hacen los alineamientos

sa = Suffix_Array(g_ref)
alineamientos = []
c=0
l=len(reads)
for x in reads:

    # Se hace matching exacto del read en tres fragmentos
    progress = "Alineado: "+str(math.floor(c/l*100))+"%"
    sys.stdout.write("\r"+progress)
    sys.stdout.flush()
    fragmento_1 = x[0:17]
    fragmento_2 = x[17:34]
    fragmento_3 = x[34:50]
    ocurrencias_1 = sa.buscar(fragmento_1)
    ocurrencias_2 = sa.buscar(fragmento_2)
    ocurrencias_3 = sa.buscar(fragmento_3)

    #Se hace el alineamiento del read
    if(len(ocurrencias_1) > 0):
        for y in ocurrencias_1:
            al = Aligner(x, g_ref[y:y+52])
            pos = al.buscar()
            if(pos <= 2):
                alineamientos.append((y + pos, x))
    elif(len(ocurrencias_2) > 0):
        for y  in ocurrencias_2:
            al = Aligner(x, g_ref[y-19:y+35])
            pos = al.buscar()
            if(pos <= 2):
                alineamientos.append((y - 19 + pos, x))
    elif(len(ocurrencias_3) > 0):
        for y  in ocurrencias_2:
            al = Aligner(x, g_ref[y-36:y+16])
            pos = al.buscar()
            if(pos <= 2):
                alineamientos.append((y-36 + pos, x))
    c+=1
    progress = "Alineado: "+str(math.floor(c/l*100))+"%"
    sys.stdout.write("\r"+progress)
print("\n")


#Se ordenan los reads segun su posicion en la cadena de referencia

alineamientos = sorted(alineamientos)


# Se rectifica los reads mal alineados debido a operaciones indel

delta = 0
for i in range(1, len(alineamientos)):
    alineamientos[i] = (alineamientos[i][0]+delta, alineamientos[i][1])
    des = -2
    alineado = False
    while des < 3 and not(alineado):
        alineado = True
        diff = alineamientos[i][0] - alineamientos[i-1][0] + des
        for x in range(0, 50 - diff):
            if(alineamientos[i][1][x] != alineamientos[i-1][1][x+diff]):
                alineado = False
                break
        if not(alineado):
            des+=1
    list(alineamientos[i])
    alineamientos[i] = (alineamientos[i][0]+des, alineamientos[i][1])
    delta += des
        

# Se crea un archivo que sirve como mapa de alineamiento (para ayudar a la visualizacion) 'alineamiento.fa'

fa = open("alineamiento.fa", "a")
delta = 0
for x in alineamientos:
    line = x[0]*" "
    line += x[1]
    fa.write(line+"\n")
fa.close()

# Se secuencian los read en el genoma de salida

g_sal = ""
c = 0
x = 0
while c < len(alineamientos):
    if(x >= alineamientos[c][0] + 50):
        c+=1
    else:
        g_sal += alineamientos[c][1][x - alineamientos[c][0]]
        x+=1


# Se  ecribe el archivo de salida 'phix_salida.fa'

fa = open("phix_salida.fa","a")
fa.write(">outputPhix\n")
for i in range(0, len(g_sal), 70):
    fa.write(g_sal[i:i+70]+"\n")
fa.close()
