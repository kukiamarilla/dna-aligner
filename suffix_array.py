import math

# Clase que almacena un arreglo de sufijos de una cadena dada

class Suffix_Array():
    
    def __init__(self, haystack):
        self.haystack = haystack
        self.array = []
        copy = self.haystack
        x = 0
        while len(copy) > 0:
            self.array.append((copy,x))
            copy = copy[1:]
            x+=1
        self.array = sorted(self.array)
        self.size = x


    # Obtiene el rango de posiciones posibles en donde se encuentra la subcadena por busqueda binaria

    def get_rango(self, rango, offset, needle):
        pi = rango[0]
        pf = rango[1]
        inicio = -1
        while pi <= pf:
            m = math.floor((pf + pi) / 2)
            if len(self.array[m][0]) > offset and self.array[m][0][offset] == needle[offset] and (m == rango[0] or len(self.array[m-1][0]) <= offset or self.array[m-1][0][offset] != needle[offset] ):
                inicio = m
                break
            else:
                if len(self.array[m][0]) > offset and self.array[m][0][offset] >= needle[offset]:
                    pf = m - 1
                else:
                    pi = m + 1
        
        if inicio == -1:
            return (-1, -1 )
        
        pi = inicio
        pf = rango[1]
        final = -1
        while pi <= pf:
            m = math.floor((pf + pi) / 2)
            if self.array[m][0][offset] == needle[offset] and ( m == rango[1] or self.array[m+1][0][offset] != needle[offset] ):
                final = m
                break
            else:
                if self.array[m][0][offset] > needle[offset]:
                    pf = m - 1
                else:
                    pi = m + 1
        return (inicio, final)


    # Obtiene la posicion de la subcadena

    def buscar(self, needle):
        if needle == '':
            return []
        offset = 0
        rango = (0, self.size - 1)
        while offset < len(needle):
            rango = self.get_rango(rango, offset, needle)
            if rango[0] == -1:
                return []
            offset += 1
        pos = []
        for x in range(rango[0], rango[1]+1):
            pos.append(self.array[x][1])
        return pos

