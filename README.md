Autores:

- Isaac Amarilla
- Natalia Cardozo
- Diego Larrea

## Instalación

Con pipenv:

```
pipenv install
pipenv shell
```

Con pip:

```
pip install numpy
```

## Requerimientos

- python 3
- pip o pipenv

## Utilización

Ejecutar:

```
python main.py
```

## Archivos

### Entrada

- phix.fa: Genoma de referencia
- reads_phix_1.fastq: Reads a alinear

### Salida

- alineamiento.fa: Mapa de alineamiento
- phix_salida.fa: Genoma de salida

## Algoritmo

1. Lectura: Se guarda el genoma de referencia del archivo phix.fa en una variable y los reads del archivo reads_phix_1.fastq en un arreglo.

2. Busqueda por índices: Se optó por usar como índice el arreglo de sufijos, cada read se parte en tres fragmentos para hacer un matching aproximado de cada read en el genoma utilizando el arreglo de sufijos.

3. Alineamiento: Una vez hecho el matching aproximado se realiza el alineamiento con el algoritmo de distancia de edición dando una holgura de dos caracteres en los extremos que no fueron alineados con el matching exacto. Esto debido a que pudieron ocurrir hasta dos inserciones en los extremos. Se guarda la posición del read y el read en una 2-tupla.

4. Ordenamiento: Se ordenan las tuplas según su posición en el genoma de referencia para facilitar el secuenciamiento.

5. Rectificación: Debido a que pudieron haber hasta 2 operaciones indel antes del solapamiento entre dos fragmentos contiguos, los fragmentos podrían estar desalineados hasta 2 posiciones. Se procede a rectificar el alineamiento de los fragmentos variando su posición entre -2 y 2, comparándolos con el fragmento anterior.

6. Salida: Se construye el genoma de salida leyendo los fragmentos en orden y saltando a la posición correspondiente del siguiente fragmento cuando se termina de leer el fragmento anterior. El genoma de salida se escribe en el archivo phix_salida.fa. Además también se construye un mapa de alineamiento de los fragmentos en el archivo alineamiento.fa.
