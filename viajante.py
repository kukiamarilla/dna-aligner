import numpy as np

# Clase que hace el alineamiento de dos cadenas

class Aligner():
    m = []
    a = ""
    b = ""
    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.m = np.full((len(a)+1, len(b)+1), 100)


    # Crea la tabla de distancia de edición

    def dist_min(self, path):
        
        for x in range(0, len(self.a) + 1):
            for y in range(0, len(self.b) + 1):
                if(x == 0):
                    self.m[x][y] = 0
                elif(y == 0):
                    self.m[x][y] = self.m[x-1][y] + 1
                else:
                    delta = int(self.a[x-1] != self.b[y-1])
                    self.m[x][y] = min(self.m[x-1][y] + 1, self.m[x][y-1] + 1, self.m[x-1][y-1] + delta)


    # Busca la posicion desde la cual se empezó un alineamiento

    def indice(self, coords):
        x=coords[0]
        y=coords[1]
        if(x == 0 or y == 0):
            return y - x
        delta = int(self.a[x-1] != self.b[y-1])
        if(self.m[x-1][y-1] == self.m[x][y] - delta):
            return self.indice((x-1,y-1))
        elif(self.m[x][y-1] == self.m[x][y] - 1):
            return self.indice((x,y-1))
        elif(self.m[x-1][y] == self.m[x][y] - 1):
            return self.indice((x-1,y))


    # Devuelve la posicion de la subsecuencia con distancia de edición mínima

    def buscar(self):
        path = (0,0)
        self.dist_min(path)
        dmin=len(self.b)
        coords=(0,0)
        for x in range(len(self.b)+1):
            if(self.m[len(self.a)][x] < dmin):
                coords = (len(self.a), x)
                dmin = self.m[len(self.a)][x]
        if(dmin > 2):
            return -1
        alin = self.indice(coords)
        return alin

